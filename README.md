# Data engineering home exercise

Proposed solution to https://github.com/MarkiesFredje/data-engineering-exercise

To be run on a Linux system with Docker installed. This was tested with WSL2/Ubuntu.

## Notes
* Geo exercise is still TO-DO
* This was fun!
* There are still areas where I'd like the solution to be cleaner but my experience with conda/pandas/... is limited so consider this an effort from scratch. And then there is life with its many time-consuming obligations...
* Conda package lists are generated, but not used at the moment. Environments are created from scratch with minimal input. Separation of environments is done at container lever, not venv.

## Quickstart

* Build: `make build`
* Start demo: `make start`
* Stop demo: `make clean`

## Demo URLs

* Cars API demo: http://127.0.0.1:5000/api/v1/cars/price_prediction?vehicleType=-1&gearbox=1&powerPS=0&model=118&kilometer=150000&monthOfRegistration=0&fuelType=1&brand=38
* Jupyter notebook: http://127.0.0.1:8888/
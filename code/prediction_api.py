import flask
from flask import request, jsonify
from joblib import load

app = flask.Flask(__name__)
app.config["DEBUG"] = False

lgbr_cars = load("/opt/models/lgbr_cars.model")

@app.route('/', methods=['GET'])
def home():
    return '''Try <a href="/api/v1/cars/price_prediction?vehicleType=-1&gearbox=1&powerPS=0&model=118&kilometer=150000&monthOfRegistration=0&fuelType=1&brand=38">this API call</a>'''

@app.route('/api/v1/cars/price_prediction', methods=['GET'])
def api_id():

    car_input=[[
        float(request.args['vehicleType']),
        float(request.args['gearbox']),
        float(request.args['powerPS']),
        float(request.args['model']),
        float(request.args['kilometer']),
        float(request.args['monthOfRegistration']),
        float(request.args['fuelType']),
        float(request.args['brand']),
    ]]

    return jsonify(lgbr_cars.predict(car_input)[0])

app.run(host='0.0.0.0')